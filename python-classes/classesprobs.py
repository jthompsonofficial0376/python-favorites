''' 
prob # 6 

Write a class that meets these requirements.

Name: Employee

Required state:

first name, a string
last name, a string
Behavior:

get_fullname: should return "«first name» «last name»"
get_email: should return "«first name».«last name»@company.com" all 
in lowercase letters
Example:

   employee = Employee("Duska", "Ruzicka")
   print(employee.get_fullname())  # prints "Duska Ruzicka"
   print(employee.get_email())     # prints "duska.ruzicka@company.com"
You may want to look at the string .lower() method to help with this code.

Do it without our helper pseudocode! You may write your own though.

Don't look at the last one you just wrote unless you really must

'''


