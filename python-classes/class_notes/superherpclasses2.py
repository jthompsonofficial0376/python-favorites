# Here come the aliens! We now also have superheros from other worlds.
# We must track what planet alien heros are from.
# Additionally, the public is really impressed by them, so the company
# can charge double for an alien hero!

class AlienHero(Superhero):
    def __init__(self, name, superpowers, villains_beaten, planet):
        super().__init__(name, superpowers, villains_beaten)
        self.planet = planet
        
    def get_planet(self):
        return self.planet
    
    def get_rate(self):
        return super().get_rate() * 2
    
class OmicronHero(AlienHero):
    def __init__(self, name, superpowers, villains_beaten):
        super().__init__(name, superpowers, villains_beaten, "Omicron")
        
    
hero4 = AlienHero("Lrrr", ["Eats anything", "Fabulous cape"], 23, "Omicron Persei 8")
print("----- " + hero4.get_planet() + " -----")
hero4.describe_yourself()