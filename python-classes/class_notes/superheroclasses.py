# I want to track superheros, but these superheros all work for a big
# corporation and charge per job! They need a name, a list of 
# superpowers, and a number of villains beaten.
# 
# You should be able to get the hero's rate...
# Rate is calculated as below:
# --- The base rate is $1,000.
# --- The more superpowers they have, the more they can charge ($1000 extra per power). 
# --- The more villains they've beaten, the more they can charge ($250 extra per villain).

class Superhero:
    base_rate = 1000
    
    def __init__(self, name, superpowers, villains_beaten):
        # initialize the state
        self.name = name
        self.superpowers = superpowers
        self.villains_beaten = villains_beatena * 2
        
    def get_rate(self):
        # get the bonus for superpowers
        superpower_bonus = len(self.superpowers) * 1000
        # get the bonus for villains
        villains_beaten_bonus = self.villains_beaten * 250
        # re-calculate the rate
        calculated_rate = self.base_rate + superpower_bonus + villains_beaten_bonus
        # return the final rate
        return calculated_rate

    def describe_yourself(self):
        # make a string out of the list of superpowers
        # print a nice description using info about the hero
        print(f"My name is {self.name} and I kick butt!")
        print(f"I've beaten {self.villains_beaten} villains with my {len(self.superpowers)} superpowers.")
        print(f"I can beat your villain for the low low price of only ${self.get_rate()}")


hero1 = Superhero("Super King", ["super strength", "good looks"], 10)
superhero_list = [hero1]
hero2 = Superhero("Crazy Bird", ["flight", "evil stare", "poop from above"], 23)
superhero_list.append(hero2)
hero3 = Superhero("Bob", ["accounting"], 4873)
superhero_list.append(hero3)

for hero in superhero_list:
    print("------------------------------------------------")
    hero.describe_yourself()







''' 
the rest of this example is using class inheritencw
too instantiate a sub - class , from the Superhero class.

'''

# Here come the aliens! We now also have superheros from other worlds.
# We must track what planet alien heros are from.
# Additionally, the public is really impressed by them, so the company
# can charge double for an alien hero!



class AlienHero(Superhero):
    def __init__(self, name, superpowers, villains_beaten, planet):
        super().__init__(name, superpowers, villains_beaten)
        self.planet = planet
        
    def get_planet(self):
        return self.planet
    
    def get_rate(self):
        return super().get_rate() * 2
    
class OmicronHero(AlienHero):
    def __init__(self, name, superpowers, villains_beaten):
        super().__init__(name, superpowers, villains_beaten, "Omicron")
        
    
hero4 = AlienHero("Lrrr", ["Eats anything", "Fabulous cape"], 23, "Omicron Persei 8")
print("----- " + hero4.get_planet() + " -----")
hero4.describe_yourself()