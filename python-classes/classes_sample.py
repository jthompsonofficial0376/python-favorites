import math


class EquilateralShape:
    def __init__(self, num_sides, side_length):
        self.num_sides = num_sides
        self.side_length = side_length

    def calculate_perimeter(self):
        result = self.num_sides * self.side_length
        return result

    def calculate_area(self):
        raise ValueError


class EquilateralTriangle(EquilateralShape):
    def __init__(self, side_length):
        super().__init__(3, side_length)

    def calculate_area(self):
        side_squared = pow(self.side_length, 2)
        return math.sqrt(3) / 4 * side_squared


class Square(EquilateralShape):
    def __init__(self, side_length):
        super().__init__(4, side_length)

    def calculate_area(self):
        return pow(self.side_length, 2)


class EquilateralPentagon(EquilateralShape):
    def __init__(self, side_length):
        super().__init__(5, side_length)

    def calculate_area(self):
        side_squared = pow(self.side_length, 2)
        weird_value = math.sqrt(25 + 10 * math.sqrt(5))
        return weird_value / 4 * side_squared


class EquilateralHexagon(EquilateralShape):
    def __init__(self, side_length):
        super().__init__(6, side_length)

    def calculate_area(self):
        side_squared = pow(self.side_length, 2)
        return (3 * math.sqrt(3)) / 2 * side_squared

print(EquilateralTriangle(6))
