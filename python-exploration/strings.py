message = "This is a string in a  python file."
some_number = 1928

print("The message is" , message) 
print("The number is" , some_number  )

number_three = 3 
string_three = "3" 
print(number_three)
print(string_three) 


print(number_three == string_three)

convertedString = string_three 

print(convertedString == string_three)


# Asking the user for a response

#response = input("How many walls are there in your room ? ")
#print("You typed:" , response)  

#print("The value type is a" , type(response))



num_walls = 6  
print("My room has", num_walls + 1 , " walls")

symbol = "*"

title = "Dr." 
name = "Syed" 
combined = title + " " + name
print(combined) 


combined2 = (symbol * 2)  + title
print(combined2)




# Custom Footer - 
print( "\n"+ "_________________" + "\n" "a script " + " that was"   )
print("  %s \n  \t%s  \n   \t      %s \n \t  \t \t %s  \n    " % ( "Coded by",  "-- Vampeyer --" , "in the year of" ,  "2024" ))



# Taking in   a string , and seting the response type to 
# a specific data type for the response 
 
age_string = input("How old are you ? ")
age = int(age_string) 
print("Next year , you'll be" , age + 1 , "years old ")



name = input("Please type your first name: ")
name_length = len(name)

if name_length > 8:
    print("You have a long name!")
else:
    print("Your name is nice and short.")





age = 23

if age > 17:
    print("You can buy a lottery ticket.")
    print("How many would you like?")
else:
    print("You may not buy a lottery ticket.")
    print("Can I interest you in some candy?")

print("Thank you for your patronage.")