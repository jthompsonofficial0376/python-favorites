'''
This code uses the ‘os' module to get and 
print the current working
 directory (CWD) of the Python script. It retrieves 
the CWD using the ‘os.getcwd()' and then prints 
it to the console.

'''
import os 
cwd = os.getcwd() 
print("Current working directory:", cwd) 

def current_path(): 
    print("Current working directory before") 
    print(os.getcwd()) 
    print() 
current_path() 

# changes directory 
os.chdir('../') 

current_path() 




'''
Creating a Directory
There are different methods available in the OS module for creating a directory. These are –

os.mkdir()
os.makedirs()
'''

directory = "GeeksforGeeks"
parent_dir = "C:/hack-reactor/python_modules"
path = os.path.join(parent_dir, directory)
 
os.mkdir(path)
print("Directory '% s' created" % directory)
directory = "Geeks"
parent_dir = "C:/hack-reactor/python_modules"
mode = 0o666
path = os.path.join(parent_dir, directory)
os.mkdir(path, mode)