n your main.py file that you created, let's write some code that uses the isleap function  of the calendar module. Take a look at that documentation to see what the parameters are and what the function returns.
 This code will tell you if the year that you respond with is a leap year.

import calendar

year_str = input("What year were you born in? ")
year = int(year_str)


writing our own modules.

Learning objectives
After reading this article, you should be able to:

Explain that a module is a collection of code
Explain that the import statement brings in a named module of code to be used with the module named prefix
Explain that the from-import statement imports specific named things from a module
Explain that Python looks for modules based on the value in sys.path
Explain that any Python file that you create is a module
Explain that you can import code from modules that you create




--------------------------------------------
if calendar.isleap(year):
    print("That was a leap year!")
else:
    print("That was a normal year.")
Run that code to make sure it works for you.
-----------------------------------------------



To use the functions in the calendar module, we write import calendar. Python will then allow you to use any of the functions in the calendar module.

The previous code specifically wants to use the isleap function. To do that, we use the dot after the module name calendar. to tell Python that we want to access something in that module. We follow the dot with the thing we want to use, calendar.isleap, telling Python that we specifically want to use the isleap method in the calendar module.

You have to import a module before using it. Try commenting out the first line by putting a # at the beginning of the line.

# import calendar
Try running that. You should get an error that includes the statement name 'calendar' is not defined. You must import the module before using it. Delete the # that you added and run the code again to make sure it all works.